#АХП

[cheatsheet](https://habr.com/ru/post/238347/) по maxima

Запускать от имени администратора в Windows.

Знак присвоения - двоеточие
`shift + enter` - вычислить ячейку
степень `^`, `^^` или `**
`%` - результат предыдущего вычисления.

## Константы
%e - постоянная Эйлера
%i - imaginary unit, _sqrt(- 1)_.
Другие - https://maths.cnam.fr/Membres/wilk/MathMax/help/Maxima/maxima_13.html


На такие штуки можно отвечать
`"Is "(lambda-mu)*(9*lambda-mu)" positive, negative or zero?"`
`positive` (`shift + enter`);
Можно просто `pos`.

Можно обращаться к n-й ячейке вывода с конца (?)
`%th(1)`

Можно обращаться к ячейке вывода по номеру
```r
%o15
```

Взятие правой части формулы
```r
rhs(%o15);
```

## Решение №1 без lt/ilt
```r
atvalue(P0(t),t=0,1)$
atvalue(P1(t),t=0,0)$
atvalue(P2(t),t=0,0)$
/* Решаем систему линейных дифференциальных уравнений */
result:desolve([
        'diff(P0(t),t)=η*P1(t)-2*λ*P0(t),
         'diff(P1(t),t)=2*λ*P0(t)+2*η*P2(t)-(λ+η)*P1(t),
         'diff(P2(t),t)=λ*P1(t)-2*η*P2(t)
    ], [
        P0(t),P1(t),P2(t)
    ]);
```

```r
limit(rhs(%[1], t, inf))
limit(rhs(%[2], t, inf))
limit(rhs(%[3], t, inf))
```


## Ошибки

`incorrect syntax: mu is not an infix operator` - пропущен знак умножения. Запись вида `5mu` должна быть представлена как `5 * mu` .



![[Maxima — Tips&Tricks, или собираем по крохам инфо, как на ней работать _ Хабр.pdf]]