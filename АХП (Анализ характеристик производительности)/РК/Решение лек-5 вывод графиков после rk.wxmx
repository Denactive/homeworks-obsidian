PK     �zV�B�H         mimetypetext/x-wxmathmlPK     �zV��R  R  
   format.txt

This file contains a wxMaxima session in the .wxmx format.
.wxmx files are .xml-based files contained in a .zip container like .odt
or .docx files. After changing their name to end in .zip the .xml and
eventual bitmap files inside them can be extracted using any .zip file
viewer.
The reason why part of a .wxmx file still might still seem to make sense in a
ordinary text viewer is that the text portion of .wxmx by default
isn't compressed: The text is typically small and compressing it would
mean that changing a single character would (with a high probability) change
big parts of the  whole contents of the compressed .zip archive.
Even if version control tools like git and svn that remember all changes
that were ever made to a file can handle binary files compression would
make the changed part of the file bigger and therefore seriously reduce
the efficiency of version control

wxMaxima can be downloaded from https://github.com/wxMaxima-developers/wxmaxima.
It also is part of the windows installer for maxima
(https://wxmaxima-developers.github.io/wxmaxima/).

If a .wxmx file is broken but the content.xml portion of the file can still be
viewed using a text editor just save the xml's text as "content.xml"
and try to open it using a recent version of wxMaxima.
If it is valid XML (the XML header is intact, all opened tags are closed again,
the text is saved with the text encoding "UTF8 without BOM" and the few
special characters XML requires this for are properly escaped)
chances are high that wxMaxima will be able to recover all code and text
from the XML file.

PK     �zVeQ��}#  }#     content.xml<?xml version="1.0" encoding="UTF-8"?>

<!--   Created using wxMaxima 22.04.0   -->
<!--https://wxMaxima-developers.github.io/wxmaxima/-->

<wxMaximaDocument version="1.5" zoom="100" activecell="9">

<cell type="code">
<input>
<editor type="input">
<line>λ:0.26$;</line>
<line>η:0.93$;</line>
<line>ν:0.38$;</line>
<line>γ:0.8$;</line>
<line>k:7$;</line>
<line></line>
<line>sol: rk([</line>
<line>        η*P1 - k*λ*P0 + γ*P_0 - ν*P0,</line>
<line>        (k)*λ*P0 + γ*P_1 + η*P2 - (η + ν + (k-1)*λ)*P1,</line>
<line>        (k-1)*λ*P1 + γ*P_2 + η*P3 - (η + ν + (k-2)*λ)*P2,</line>
<line>        (k-2)*λ*P2 + γ*P_3 + η*P4 - (η + ν + (k-3)*λ)*P3,</line>
<line>        (k-3)*λ*P3 + γ*P_4 + η*P5 - (η + ν + (k-4)*λ)*P4,</line>
<line>        (k-4)*λ*P4 + γ*P_5 + η*P6 - (η + ν + (k-5)*λ)*P5,</line>
<line>        (k-5)*λ*P5 + γ*P_6 + η*P7 - (η + ν + (k-6)*λ)*P6,</line>
<line>        (k-6)*λ*P6 + γ*P_7 + η*P8 - (η + ν + (k-7)*λ)*P7,</line>
<line>        /* (k-7)*λ*P7 + γ*P_8 + η*P9 - (η + ν + (k-8)*λ)*P8, */</line>
<line>        /* (k-8)*λ*P8 - η*P9 - ν*P9, */</line>
<line>        </line>
<line>        0,</line>
<line>        0,</line>
<line>        </line>
<line>        ν*P0 - γ*P_0 - k*λ*P_0,</line>
<line>        (k)*λ*P_0 + ν*P1 - (γ + (k-1)*λ)*P_1,</line>
<line>        (k-1)*λ*P_1 + ν*P2 - (γ + (k-2)*λ)*P_2,</line>
<line>        (k-2)*λ*P_2 + ν*P3 - (γ + (k-3)*λ)*P_3,</line>
<line>        (k-3)*λ*P_3 + ν*P4 - (γ + (k-4)*λ)*P_4,</line>
<line>        (k-4)*λ*P_4 + ν*P5 - (γ + (k-5)*λ)*P_5,</line>
<line>        (k-5)*λ*P_5 + ν*P6 - (γ + (k-6)*λ)*P_6,</line>
<line>        (k-6)*λ*P_6 + ν*P7 - (γ + (k-7)*λ)*P_7,</line>
<line>        /* (k-7)*λ*P_7 + ν*P8 + ν*P9, */</line>
<line>        </line>
<line>        0</line>
<line>    ], [</line>
<line>        P0,P1,P2,P3,P4,P5,P6,P7,P8,P9,</line>
<line>        /* P8 , P9, */</line>
<line>        P_0,P_1,P_2,P_3,P_4,P_5,P_6,P_7,P_8</line>
<line>        /* P_8 */</line>
<line>    ], [</line>
<line>        1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  /* 2m + 3 = 19 штук*/</line>
<line>    ], [</line>
<line>        t, 0, 15, 0.02</line>
<line>    ])$</line>
<line></line>
<line>    </line>
</editor>
</input>
</cell>

<cell type="code">
<input>
<editor type="input">
<line></line>
<line>   </line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o34)	">(%o34) </lbl><t>false</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line></line>
<line>t: 0.762731856433675$</line>
<line>plot2d([</line>
<line>        [discrete, makelist([p[1], p[2]], p, sol)],</line>
<line>        [discrete, [[t,0], [t,0.31]]]</line>
<line>    ], [xlabel, &quot;t&quot;], [ylabel, &quot;P0&quot;]</line>
<line>);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o134)	">(%o134) </lbl><t>false</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>plot2d([[discrete, makelist([p[1], p[3]], p, sol)], [discrete, [[t,0], [t,0.31]]]], [xlabel, &quot;t&quot;], [ylabel, &quot;P1&quot;]);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o144)	">(%o144) </lbl><t>false</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>plot2d([[discrete, makelist([p[1], p[4]], p, sol)], [discrete, [[t,0], [t,0.31]]]], [xlabel, &quot;t&quot;], [ylabel, &quot;P2&quot;]);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o136)	">(%o136) </lbl><t>false</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>plot2d([[discrete, makelist([p[1], p[20]], p, sol)], [discrete, [[t,0], [t,0.31]]]], [xlabel, &quot;t&quot;], [ylabel, &quot;P&apos;8&quot;], [x, 0, 15], [y, 0, 0.5]);</line>
<line></line>
<line></line>
<line></line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o163)	">(%o163) </lbl><t>false</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>/* пары точек х/y для графика P3 */</line>
<line>/*makelist([p[1], p[5]], p, sol); */</line>
<line>/* последняя такая точка при х = 15*/</line>
<line>   last(makelist([p[1], p[5]], p, sol));</line>
<line>/* коорината y такой точки при х = 15 без вывода координаты х*/</line>
<line>   last(makelist(p[5], p, sol));</line>
<line>/* вывод всех последних координат у для функций P0(t)...P&apos;m(t)*/</line>
<line> makelist(</line>
<line>    last(makelist(p[i], p, sol)),</line>
<line>    i,</line>
<line>    2,</line>
<line>    20</line>
<line>);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o158)	">(%o158) </lbl><r list="true"><t listdelim="true">[</t><n>15.0</n><fnm>,</fnm><n>0.09788421039339801</n><t listdelim="true">]</t></r><lbl altCopy="(%o159)	">(%o159) </lbl><n>0.09788421039339801</n><lbl altCopy="(%o160)	">(%o160) </lbl><r list="true"><t listdelim="true">[</t><n>0.012833529560874</n><fnm>,</fnm><n>0.02842939671168107</n><fnm>,</fnm><n>0.05678084077592044</n><fnm>,</fnm><n>0.09788421039339801</n><fnm>,</fnm><n>0.1402360857876071</n><fnm>,</fnm><n>0.1582435318142673</n><fnm>,</fnm><n>0.1272765463197673</n><fnm>,</fnm><n>0.05628196695187609</n><fnm>,</fnm><n>0.0</n><fnm>,</fnm><n>0.0</n><fnm>,</fnm><n>0.001879417979166265</n><fnm>,</fnm><n>0.006087920954587321</n><fnm>,</fnm><n>0.01494167816405481</n><fnm>,</fnm><n>0.03103670449247362</n><fnm>,</fnm><n>0.05449914493244869</n><fnm>,</fnm><n>0.07792056074670196</n><fnm>,</fnm><n>0.08331078072097839</n><fnm>,</fnm><n>0.05235768369419638</n><fnm>,</fnm><n>0.0</n><t listdelim="true">]</t></r>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line> sol: rk([</line>
<line>        η*P1 - k*λ*P0 + γ*P_0 - ν*P0,</line>
<line>        (k)*λ*P0 + γ*P_1 + η*P2 - (η + ν + (k-1)*λ)*P1,</line>
<line>        (k-1)*λ*P1 + γ*P_2 + η*P3 - (η + ν + (k-2)*λ)*P2,</line>
<line>        (k-2)*λ*P2 + γ*P_3 + η*P4 - (η + ν + (k-3)*λ)*P3,</line>
<line>        (k-3)*λ*P3 + γ*P_4 + η*P5 - (η + ν + (k-4)*λ)*P4,</line>
<line>        (k-4)*λ*P4 + γ*P_5 + η*P6 - (η + ν + (k-5)*λ)*P5,</line>
<line>        (k-5)*λ*P5 + γ*P_6 + η*P7 - (η + ν + (k-6)*λ)*P6,</line>
<line>        (k-6)*λ*P6 + γ*P_7 + η*P8 - (η + ν + (k-7)*λ)*P7,</line>
<line>        /* (k-7)*λ*P7 + γ*P_8 + η*P9 - (η + ν + (k-8)*λ)*P8, */</line>
<line>        /* (k-8)*λ*P8 - η*P9 - ν*P9, */</line>
<line>        </line>
<line>        0,</line>
<line>        0,</line>
<line>        </line>
<line>        ν*P0 - γ*P_0 - k*λ*P_0,</line>
<line>        (k)*λ*P_0 + ν*P1 - (γ + (k-1)*λ)*P_1,</line>
<line>        (k-1)*λ*P_1 + ν*P2 - (γ + (k-2)*λ)*P_2,</line>
<line>        (k-2)*λ*P_2 + ν*P3 - (γ + (k-3)*λ)*P_3,</line>
<line>        (k-3)*λ*P_3 + ν*P4 - (γ + (k-4)*λ)*P_4,</line>
<line>        (k-4)*λ*P_4 + ν*P5 - (γ + (k-5)*λ)*P_5,</line>
<line>        (k-5)*λ*P_5 + ν*P6 - (γ + (k-6)*λ)*P_6,</line>
<line>        (k-6)*λ*P_6 + ν*P7 - (γ + (k-7)*λ)*P_7,</line>
<line>        /* (k-7)*λ*P_7 + ν*P8 + ν*P9, */</line>
<line>        </line>
<line>        0</line>
<line>    ], [</line>
<line>        P0,P1,P2,P3,P4,P5,P6,P7,P8,P9,</line>
<line>        /* P8 , P9, */</line>
<line>        P_0,P_1,P_2,P_3,P_4,P_5,P_6,P_7,P_8</line>
<line>        /* P_8 */</line>
<line>    ], [</line>
<line>        1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  /* 2m + 3 = 19 штук*/</line>
<line>    ], [</line>
<line>        x, 0, t, 0.02</line>
<line>    ])$</line>
</editor>
</input>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>makelist(</line>
<line>    last(makelist(p[i], p, sol)),</line>
<line>    i,</line>
<line>    2,</line>
<line>    20</line>
<line>);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o167)	">(%o167) </lbl><r list="true"><t listdelim="true">[</t><n>0.297003741872321</n><fnm>,</fnm><n>0.2911974954235846</n><fnm>,</fnm><n>0.1573090022128097</n><fnm>,</fnm><n>0.05145831723707577</n><fnm>,</fnm><n>0.01051137385121081</n><fnm>,</fnm><n>0.001316632709441987</n><fnm>,</fnm><n>9.283325023345121</n><h>·</h><e><r><n>10</n></r><r><n>−5</n></r></e><fnm>,</fnm><n>2.829245460549245</n><h>·</h><e><r><n>10</n></r><r><n>−6</n></r></e><fnm>,</fnm><n>0.0</n><fnm>,</fnm><n>0.0</n><fnm>,</fnm><n>0.05588611148197345</n><fnm>,</fnm><n>0.07264728507129967</n><fnm>,</fnm><n>0.04377172959914946</n><fnm>,</fnm><n>0.01515673151707628</n><fnm>,</fnm><n>0.00320479731686404</n><fnm>,</fnm><n>4.10770531003821</n><h>·</h><e><r><n>10</n></r><r><n>−4</n></r></e><fnm>,</fnm><n>2.944042818760312</n><h>·</h><e><r><n>10</n></r><r><n>−5</n></r></e><fnm>,</fnm><n>9.082523076997561</n><h>·</h><e><r><n>10</n></r><r><n>−7</n></r></e><fnm>,</fnm><n>0.0</n><t listdelim="true">]</t></r>
</mth></output>
</cell>

</wxMaximaDocument>PK      �zV�B�H                       mimetypePK      �zV��R  R  
             5   format.txtPK      �zVeQ��}#  }#               �  content.xmlPK      �   U*    