# Лабораторная работа 3. Модель однофазной одноканальной замкнутой системы обслуживания

# Задание 1
# В процессе эксплуатации ЭВМ осуществляются ее разовые проверки, 
# в результате которых она может оказаться в следующих состояниях:
# - S1 - ЭВМ полностью исправна;
# - S2 - ЭВМ имеет незначителльные неисправности в программном обеспечении, 
# при которых она может решать задачи;
# - S3 - ЭВМ имеет существенные неисправности и может решать ограниченный класс задач;
# - S4 - ЭВМ полностью вышла из строя.

# В начальный момент времени ЭВМ полностью исправна (состояние S1). 
# Проверка ЭВМ производится в фиксированные моменты времени tk. 
# Процесс, протекающий в системе, может рассматриваться как однородная марковская цепь 
# с k шагами (1, 2, ..., k проверки). 

# Матрица переходных вероятностей имеет вид:
# P = 
# p11 p12 p13 p14
# 0   p22 p23 p24 
# 0   0   p33 p34 
# 0   0   0   p44

# Построить граф состояний. Найти вероятности (численно и теоретически) состояний ЭВМ после k-2, k-1, k осмотров.

install.packages("lpSolve")
install.packages("matrixcalc")
library(matrixcalc)

Variant<-9
set.seed(Variant)
k<-sample(c(4:9),1)
pp1<-runif(4)
pp2<-runif(3)
pp3<-runif(2)
p1<-pp1/sum(pp1)
p2<-c(c(0),pp2/sum(pp2))
p3<-c(c(0,0),pp3/sum(pp3))
p4<-c(0,0,0,1)
P<-data.frame()
P<-rbind(P,p1)
P<-rbind(P,p2)
P<-rbind(P,p3)
P<-rbind(P,p4)
rownames(P)<-c("p1","p2","p3","p4")
colnames(P)<-c("","","","")
View(P)
print(paste("k=",as.character(k)))

statesCount<-4
ks<-c(k-2, k-1, k)

# экспериментально

# StateRand - генерирует номер нового состояния
# s - состояние, в котором находимся
StateRand<-function(s) {
  r = runif(1)
  sum <- 0
  i <-0
  while ((i < statesCount) & (sum < r)) {
    i <- i + 1
    sum <- sum + M[s,i]
  }
  return(i)
}
# эксперимент для к осмотров
Exp1Func<-function(k) {
  N <- 1000000
  exp <- vector(mode="integer", length = 4)
  for (i in 1:N)
  {
    state = 1
    for (i in 1:k) {
      state = StateRand(state)  
    }
    exp[state] = exp[state] + 1
  }
  
  resExp <- vector(mode="integer", length = 4)
  for (i in 1:length(exp)) {
    resExp[i] = exp[i] / N
  }
  
  return (resExp)
}


# теоретически
Teor1Func<-function(k) {
  M<-matrix(c(p1,
              p2,
              p3,
              p4), ncol=4, nrow=4, byrow=TRUE)
  # изначально в состоянии S1
  M0<-matrix(c(1, 0, 0, 0), ncol=4, nrow=1, byrow=TRUE)
  # M0 * M^k
  resTeor = M0 %*% matrix.power(M,k)
  return (resTeor) 
}


ksNames = c("- 2", "- 1", "")
results1 = data.frame(rows.names=c("s1","s2","s3","s4"))
for (i in 1:length(ks)) {
  res1Exp = Exp1Func(ks[i])
  res1Teor = Teor1Func(ks[i])
  
  results1[paste('Численно, для k', ksNames[i], '=', ks[i])] = c(res1Exp)
  results1[paste('Теоретически для k', ksNames[i], '=', ks[i])] = c(res1Teor)
}

View(results1)

# -=======================================================================================================================-
# Задание 2
# Вычислительный центр фирмы состоит из одного главного сервера коллективного пользования. 
# Число работающих в центре программистов в любой момент времени равно k. 
# Каждый программист готовит свою программу и через терминал передает ее на сервер для выполнения, куда она сразу попадает. 
# Время подготовки программ имеет экспоненциальное распределение со средним значением t1 мин. 
# Время выполнения программы имеет экспоненциальное распределение со средним значением t2 мин. 
# Найти (теоретически и экспериментально):
# - вероятность того, что программа не будет выполнена сразу же, как только она поступила на терминал;
# - среднее время до получения пользователем результатов реализации;
# - среднее количество программ, ожидающих выполнения на сервере.
Variant<-9
set.seed(Variant)
k<-sample(c(10:25),1)
t1<-sample(c(14:20),1)
t2<-sample(c(2:5),1)
View(data.frame(k,t1,t2))

# если lambda >= mu, очередь растет до бесконечности (см. теоретическое решение)
# увеличим, например, время подготовки программ (преподаватель сказал поменять t1 или t2)
t1 <- 100

# экспериментально

DelFirst<-function(array) {
  newArr <- c()
  if (length(array) > 1) {
    newArr = array[2:length(array)]
  } else {
    newArr = c()
  }
  return (newArr)
}

N<-50000

timesPrepare<-rexp(N,1/t1)
timesExecute<-rexp(N,1/t2)
timesPrepare
timesExecute

# кол-во программ, выполнимых сразу же, как только поступят в очередь
executeAtOnce<-0
executesAtOnce<-c()
# время в очереди
timesWait<-c()

# время, оставшееся для завершения подготовки программ для k программистов
timesProgrammers<-sort(timesPrepare[1:k])
timesProgrammers

prepareNumber<-k+1
executeNumber<-1

# время, оставшееся для завершения выполнения программ в очереди
tExecutes<-c()

for (i in 1:N) {
  # время до поступления очередной программы и поставновки и её в очередь
  tPrepare<-0
  
  print(i)
  print("Текущее время подготовки программ:")
  print(timesProgrammers)
  
  # подготавливается 1 программа, оставшееся время подготовки которой меньше всего
  if (length(timesProgrammers) != 0) {
    tPrepare = timesProgrammers[1]
  }
  print("ВРЕМЯ ПОДГОТОВКИ:")
  print(tPrepare)
  

  # у остальных программ время подготовки уменьшается на время подготовки этой программы
  if (length(timesProgrammers) != 0) {
    timesProgrammers[1] = 0 # можно убрать
    if (length(timesProgrammers) > 1) {
      for (j in 2:length(timesProgrammers)) {
        timesProgrammers[j] = timesProgrammers[j] - tPrepare
      }
    }
  }
  print("Время подготовки для остальных программ:")
  print(timesProgrammers)
  
  # поступает следующая программа, если программы еще не закончились
  if (prepareNumber <= N) {
    timesProgrammers = c(timesPrepare[prepareNumber], timesProgrammers[2:k])
    prepareNumber = prepareNumber + 1
    timesProgrammers = sort(timesProgrammers)
  } else {
    timesProgrammers = DelFirst(timesProgrammers)
  }
  print("Поступление новой программы:")
  print(timesProgrammers)
  

  # за время tPrepare выполняются программы, если они есть
  while (length(tExecutes) > 0 && tExecutes[1] < tPrepare) {
    tPrepare = tPrepare - tExecutes[1]
    tExecutes = DelFirst(tExecutes)
  }
  if (length(tExecutes) > 0) {
    tExecutes[1] = tExecutes[1] - tPrepare
  }
  print("Оставшиеся выполняться (после времени tPrepare) программы в очереди:")
  print(tExecutes)
  
  # вычисления по заданию
  # при поступлении программы в очередь время ее ожидания = сумме времен ожидания всех программ в очереди
  timesWait = append(timesWait, 0)
  if (length(tExecutes) > 0) {
    timesWait[i] = sum(tExecutes)
  }
  
  if (length(tExecutes) == 0) {
    executeAtOnce = executeAtOnce + 1
    executesAtOnce = append(executesAtOnce, 1)
  } else {
    executesAtOnce = append(executesAtOnce, 0)
  }

  # программа ставится в очередь, если она есть (её может не быть в конце дня)
  if (tPrepare != 0) {
    tExecutes = append(tExecutes, timesExecute[executeNumber])
    executeNumber = executeNumber + 1
  }
  print("Выполняющиеся программы в очереди:")
  print(tExecutes)
  
  
  # программы больше не поступают, значит те, что в очереди, просто выполним
  if (executeNumber > N && length(tExecutes) > 0) {
    for (q in 1:length(tExecutes)) {
      tExecutes = DelFirst(tExecutes)
    }
    
    print("КОНЕЦ ДНЯ")
    print(tExecutes)
  }
}

# Вероятность того, что программа не будет выполнена сразу же, как только она поступила на терминал
PNoEmptyQueue<-(N-executeAtOnce)/N

# Cреднее время до получения пользователем результатов реализации (время в очереди + время выполнения программы на сервере
timeWaitAndExecute_exp<-(sum(timesWait)+sum(timesExecute))/N

# среднее время ожидания в очереди (Wоч)
timeWait_exp = sum(timesWait)/length(timesWait)
# среднее время между поступлениями программ
timeProgramsAverage = (sum(timesPrepare) / length(timesPrepare)) / k
# Wоч*/timePrepareAverage - средняя длина очереди / среднее время поступления программ
queueLength_exp<-timeWait_exp/timeProgramsAverage


# теоретически
# lambda = 1/t1, mu = 1/t2
# среднее кол-во заявок, поступающее за единицу времени t1 для k программистов (интенсивность поступления программ)

lambda <- k/t1
# среднее кол-во заявок, поступающее за единицу времени t2 (интенсивность выполнения программ)
mu <- 1/t2
# отношение интенсивности поступления к интенсивности выполнения
# p - вероятность того, что очередь будет занята
p <- lambda/mu

# cреднее время ожидания обслуживания (стояния в очереди)
Wqueue <- p^2 / (lambda*(1-p))
# время получения результатов (время стояния в очереди + время выполнения программы)
timeWaitAndExecute_teor <- t2 + Wqueue

# средняя длина очереди Lоч
queueLength_teor = p^2 / (1-p)
if (queueLength_teor < 0) {
  queueLength_teor <- 'Очередь растёт до бесконечности'
}


# ИТОГИ
results2 = data.frame(rows.names=c("P(оч!=0)","t2+Woч","Lоч"))
results2['Численно'] = c(PNoEmptyQueue, timeWaitAndExecute_exp, queueLength_exp)
results2['Теоретически'] = c(p,timeWaitAndExecute_teor,queueLength_teor)

View(results2)

# -=======================================================================================================================-

# дополнительное задание
# Точка блуждает по оси абсцисс: на каждом шаге она с вероятностью p1 остается на месте, 
# с вероятностью p2 перескакивает на единицу вправо и с вероятностью p3 влево. Состояние системы  после k шагов 
# определяется одной координатой (абсциссой) точки. Рассматривая последовательность положений точки как цепь Маркова, 
# найти вероятность (численно и теоретически) того, что она после k шагов окажется от начала координат не дальше, 
# чем на расстоянии, равном m.

Variant<-9
set.seed(Variant) 
pp<-runif(3)
p1<-pp[1]/sum(pp)
p2<-pp[2]/sum(pp)
p3<-pp[3]/sum(pp)
k<-sample(c(4:8),1)
m<-sample(c(1:k),1)
View(data.frame(p1,p2,p3,k,m))
p<-c(p1,p2,p3)

# экспериментально
pointStatesCount<-3
PointStateRand<-function(p) {
  r = runif(1)
  sum <- 0
  i <-0
  while ((i < pointStatesCount) & (sum < r)) {
    i <- i + 1
    sum <- sum + p[i]
  }
  return(i)
}

N <- 1000
# кол-во раз, когда точка будет на расстоянии <= m
KNormDist_exp <- 0
for (i in 1:N)
{
  # начальная координата
  x <- 0 
  for (i in 1:k) {
    state = PointStateRand(p)
    if (state == 2) {
      x = x + 1
    }
    if (state == 3) {
      x = x - 1
    }
  }
  if (abs(x) <= m) {
    KNormDist_exp = KNormDist_exp + 1
  }
  xs = append(xs, abs(x))
}
PNormDist_exp<-KNormDist_exp/N

# теоретически
# от -k до k - номера состояний (кол-во состояний 2k+1)
#      -k -k+1 -k+2 ... -1 0 1 ... k-1 k
# -k   p1  p2   0   ...  0 0 0     0   0
# -k+1 p3  p1   p2  ...  0 0 0     0   0
# ...   ...   ...   ...   ...   ...   ...
# k    0   0    0   ...  0 0 0     p3  p1 

size<-(2*k+1)
M <- matrix(0, nrow = size, ncol = size)
for (i in 1:size) {
  for (j in 1:size) {
    if (i == j) {
      M[i,j] = p1
    }
    if ((i - j) == 1) {
      M[i,j] = p3
    }
    if ((j - i) == 1) {
      M[i,j] = p2
    }
  }
}

v0<-seq(0, 0, length.out=size)
# изначально в состоянии "х=0"
v0[k+1] = 1

M0<-matrix(v0, ncol=size, nrow=1, byrow=TRUE)
# M0 * M^k
Pres_teor <- M0 %*% matrix.power(M,k)
PNormDist_teor<-0
normSize<-(2*m+1)
for (i in 1:normSize) {
  PNormDist_teor = PNormDist_teor + Pres_teor[k-m+i]
}


results3 = data.frame("Экспериментально" = PNormDist_exp, "Теоретически" = PNormDist_teor)
View(results3)

# -=======================================================================================================================-
View(results1)
View(results2)
View(results3)
